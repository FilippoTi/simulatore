/*
    0        -> END

    80       -> INT 80h

    100      -> MOVRR
    101      -> MOVRM
    102      -> MOVMR
    103      -> MOVMD
    104      -> MOVRD

    200      -> ADDRR
    201      -> ADDRM
    202      -> ADDRD
    
    250      -> SUBRR
    251      -> SUBRM
    252      -> SUBRD
    
    300      -> INCR

    350      -> DECR
    
    400      -> JMP

    10000    -> Eax
    10001    -> Ebx
    10002    -> Ecx
    10003    -> Edx
 */