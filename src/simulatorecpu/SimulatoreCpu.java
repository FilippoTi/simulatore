/*
    0        -> END

    100      -> MOVRR
    101      -> MOVRM
    102      -> MOVMR
    103      -> MOVMD
    104      -> MOVRD

    200      -> ADDRR
    201      -> ADDRM
    202      -> ADDRD
    
    250      -> SUBRR
    251      -> SUBRM
    252      -> SUBRD
    
    300      -> INCR

    350      -> DECR
    
    400      -> JMP
 */
package simulatorecpu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimulatoreCpu
{

    static int Programma[] = new int[10005];
    static int Memoria[] = new int[10005];
    static int Registri[] = new int[10005];

    static int ProgramCounter = 0;
    static boolean SignFlag = false;
    static boolean ZeroFlag = false;

    public static int Caricamento()
    {
        try
        {
            FileReader FileSorgente = new FileReader("Sorgente.dat");
            Scanner LettoreDaFile = new Scanner(FileSorgente);
            ProgramCounter = 0;

            while (LettoreDaFile.hasNextInt())
            {
                Programma[ProgramCounter] = LettoreDaFile.nextInt();
                ProgramCounter++;
            }

            ProgramCounter = 0;
            return 0;
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(SimulatoreCpu.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public static void Disassembler()
    {
        int i;
        int ArrayCopia[];
        ArrayCopia = Programma.clone();

        try
        {
            FileOutputStream f = new FileOutputStream("Disassemblato.dat");
            PrintStream fw = new PrintStream(f);

            for (i = 0; i < ArrayCopia.length; i++)
            {
                if (ArrayCopia[i] == 0) //END
                {
                    fw.println("END");
                    break;
                } else if (ArrayCopia[i] == 80) //INT 80h
                {
                    fw.println("INT 80h");
                } else if (ArrayCopia[i] == 100) //MOVRR
                {
                    fw.println("MOVRR " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 101) //MOVRM
                {
                    fw.println("MOVRM " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 102) //MOVMR
                {
                    fw.println("MOVMR " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 103) //MOVMD
                {
                    fw.println("MOVMD " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 104) //MOVRD
                {
                    fw.println("MOVRD " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 200) //ADDRR
                {
                    fw.println("ADDRR " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 201) //ADDRM
                {
                    fw.println("ADDRM " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 202) //ADDRD
                {
                    fw.println("ADDRD " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 250) //SUBRR
                {
                    fw.println("SUBRR " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 251) //SUBRM
                {
                    fw.println("SUBRM " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 252) //SUBRD
                {
                    fw.println("SUBRD " + ArrayCopia[i + 1] + ", " + ArrayCopia[i + 2]);
                    i += 2;
                } else if (ArrayCopia[i] == 300) //INCR
                {
                    fw.println("INCR " + ArrayCopia[i + 1]);
                    i += 1;
                } else if (ArrayCopia[i] == 350) //DECR
                {
                    fw.println("DECR " + ArrayCopia[i + 1]);
                    i += 1;
                } else if (ArrayCopia[i] == 400) //JMP
                {
                    fw.println("JMP " + ArrayCopia[i + 1]);
                } else if (ArrayCopia[i] == 401) //JZ
                {
                    fw.println("JZ " + ArrayCopia[i + 1]);
                } else if (ArrayCopia[i] == 402) //JNZ
                {
                    fw.println("JNZ " + ArrayCopia[i + 1]);
                } else if (ArrayCopia[i] == 403) //JS
                {
                    fw.println("JS " + ArrayCopia[i + 1]);
                } else if (ArrayCopia[i] == 404) //JNS
                {
                    fw.println("JNS " + ArrayCopia[i + 1]);
                } else
                {
                    fw.println("CODICE NON VALIDO! ");
                }

            }
            fw.close();
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(SimulatoreCpu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static int Esecuzione()
    {
        ProgramCounter = 0;
        while (1 == 1)
        {
            if (Programma[ProgramCounter] == 0) //END
            {
                System.out.println("END");
                ProgramCounter++;
                break;
            } else if (Programma[ProgramCounter] == 80) //INT 80h
            {
                // System.out.println("INT 80h");
                if (Registri[10000] == 4 && Registri[10001] == 1)
                {
                    System.out.println((char) Registri[10002]);
                }

                ProgramCounter += 1;
            } else if (Programma[ProgramCounter] == 100) //MOVRR
            {
                Registri[Programma[ProgramCounter + 1]] = Registri[Programma[ProgramCounter + 2]];
//                System.out.println("MOVRR " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 101) //MOVRM
            {
                Registri[Programma[ProgramCounter + 1]] = Memoria[Programma[ProgramCounter + 2]];
//                System.out.println("MOVRM " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 102) //MOVMR
            {
                Memoria[Programma[ProgramCounter + 1]] = Registri[Programma[ProgramCounter + 2]];
//                System.out.println("MOVMR " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 103) //MOVMD
            {
                Memoria[Programma[ProgramCounter + 1]] = Programma[ProgramCounter + 2];
                //System.out.println("MOVMD " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 104) //MOVRD
            {
                Registri[Programma[ProgramCounter + 1]] = Programma[ProgramCounter + 2];
                //System.out.println("MOVRD " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 200) //ADDRR
            {
                Registri[Programma[ProgramCounter + 1]] += Registri[Programma[ProgramCounter + 2]];
                if ((Registri[Programma[ProgramCounter + 1]] + Registri[Programma[ProgramCounter + 2]]) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] + Registri[Programma[ProgramCounter + 2]]) > 0)
                {
                    SignFlag = false;
                }
                //System.out.println("ADDRR " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 201) //ADDRM
            {
                Registri[Programma[ProgramCounter + 1]] += Memoria[Programma[ProgramCounter + 2]];
                if ((Registri[Programma[ProgramCounter + 1]] + Memoria[Programma[ProgramCounter + 2]]) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] + Memoria[Programma[ProgramCounter + 2]]) > 0)
                {
                    SignFlag = false;
                }
                //System.out.println("ADDRM " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 202) //ADDRD
            {
                Registri[Programma[ProgramCounter + 1]] += Programma[ProgramCounter + 2];
                if ((Registri[Programma[ProgramCounter + 1]] + Programma[ProgramCounter + 2]) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] + Programma[ProgramCounter + 2]) > 0)
                {
                    SignFlag = false;
                }
                // System.out.println("ADDRD " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 250) //SUBRR
            {
                Registri[Programma[ProgramCounter + 1]] -= Registri[Programma[ProgramCounter + 2]];
                if ((Registri[Programma[ProgramCounter + 1]] - Registri[Programma[ProgramCounter + 2]]) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] - Registri[Programma[ProgramCounter + 2]]) > 0)
                {
                    SignFlag = false;
                }
                // System.out.println("SUBRR " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 251) //SUBRM
            {
                Registri[Programma[ProgramCounter + 1]] -= Memoria[Programma[ProgramCounter + 2]];
                if ((Registri[Programma[ProgramCounter + 1]] - Memoria[Programma[ProgramCounter + 2]]) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] - Memoria[Programma[ProgramCounter + 2]]) > 0)
                {
                    SignFlag = false;
                }
                // System.out.println("SUBRM " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 252) //SUBRD
            {
                Registri[Programma[ProgramCounter + 1]] -= Programma[ProgramCounter + 2];
                if ((Registri[Programma[ProgramCounter + 1]] - Programma[ProgramCounter + 2]) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] - Programma[ProgramCounter + 2]) > 0)
                {
                    SignFlag = false;
                }
                // System.out.println("SUBRD " + Programma[ProgramCounter + 1] + ", " + Programma[ProgramCounter + 2]);
                ProgramCounter += 3;
            } else if (Programma[ProgramCounter] == 300) //INCR
            {
                Registri[Programma[ProgramCounter + 1]]++;
                if ((Registri[Programma[ProgramCounter + 1]] + 1) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] + 1) > 0)
                {
                    SignFlag = false;
                }
                // System.out.println("INCR " + Programma[ProgramCounter + 1]);
                ProgramCounter += 2;
            } else if (Programma[ProgramCounter] == 350) //DECR
            {
                Registri[Programma[ProgramCounter + 1]]--;
                if ((Registri[Programma[ProgramCounter + 1]] - 1) == 0)
                {
                    ZeroFlag = true;
                } else if ((Registri[Programma[ProgramCounter + 1]] - 1) > 0)
                {
                    SignFlag = false;
                }
                //System.out.println("DECR " + Programma[ProgramCounter + 1]);
                ProgramCounter += 2;
            } else if (Programma[ProgramCounter] == 400) //JMP
            {
                ProgramCounter = Programma[ProgramCounter + 1];
                //System.out.println("JMP " + Programma[ProgramCounter + 1]); s
            } else if (Programma[ProgramCounter] == 401) //JZ
            {
                if (ZeroFlag == true)
                {
                    ProgramCounter = Programma[ProgramCounter + 1];
                    //System.out.println("JMP " + Programma[ProgramCounter + 1]);
                }
            } else if (Programma[ProgramCounter] == 402) //JNZ
            {
                if (ZeroFlag == false)
                {
                    ProgramCounter = Programma[ProgramCounter + 1];
                    //System.out.println("JMP " + Programma[ProgramCounter + 1]);
                }
            } else if (Programma[ProgramCounter] == 403) //JS
            {
                if (SignFlag == true)
                {
                    ProgramCounter = Programma[ProgramCounter + 1];
                    //System.out.println("JMP " + Programma[ProgramCounter + 1]);
                }
            } else if (Programma[ProgramCounter] == 404) //JNS
            {
                if (SignFlag == false)
                {
                    ProgramCounter = Programma[ProgramCounter + 1];
                    //System.out.println("JMP " + Programma[ProgramCounter + 1]);
                }
            } else
            {
                System.err.println("CODICE NON VALIDO! ");
            }
        }

        return 0;
    }

    public static void main(String[] args)
    {
        Caricamento();
        Disassembler();
        Esecuzione();
    }
}
